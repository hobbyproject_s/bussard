return {
   [-1]                               = "manual.msg",
   [0]                                = "strauss-1.msg",
   [1200]                             = "passenger1.msg",
   [2000]                             = "passenger2.msg",
   [12000]                            = "passenger3.msg",
   [{"rot13-decrypt-accept", 600}]    = "nari-decrypt-03.msg",
   [{"passenger2", 1200}]             = "subnet.msg",
   [{"passenger2", 2200}]             = "subnet2.msg",
   [{"subnet", 10}]                   = "subnet3.msg",
}
