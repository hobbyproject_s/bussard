# Back-story notes (aka SPOILERS)

"An adventure game is a crossword at war with a narrative."
- Graham Nelson

Here's where the notes are kept in order to keep a consistent
back-story in the Bussard universe. If you're not working on
developing the game, you probably don't want to read this.

See the [backstory timeline](history.md) and
[list of characters, worlds, and organizations](characters.md).

## Guidelines

* Hard science where possible (see Crimes against Science below, don't add to that list)
* No "classic" evil villains; antagonists arise from [systemic factors](https://slatestarcodex.com/2014/07/30/meditations-on-moloch/)
 * Everyone thinks they are doing the best they can and should at least be possible to sympathize with
* Your ship cannot be destroyed; there should be no way to permanently fail
* Subnet messages use at least from, to, subject, content-type, message-id headers, page-break-separated
* Use metric time for most relative amounts, years for absolute events
* Don't make newsgroup postings unrealistically knowledgeable
* Or unrealistically polite
* If referencing current events, be sure there's a record of it here for consistency
* Questions that don't get answered are fine
* It's fine for newsgroup posters to be mistaken about small things, but the correction should usually be present in the thread
 * It's OK if it's not obvious which of two opposing views presented are correct
* Widespread mistaken views about big things should usually be part of major plot points

## Crimes against Science

* Portals
* Collisions are impossible (explained in-game by nav computer safety features)
* Bussard collector refuels reaction mass way too quickly
* Arguably the [high thrust](http://www.projectrho.com/public_html/rocket/torchships.php)
  with which you zoom around the system could be on this list; however
  we explain it in-game by showing time go by at a 1000x rate. This
  factor is not quite enough to explain crossing the whole solar system in
  under a minute, but it blurs the lines enough for our purposes.

The fact that exoplanets are colonized at all could be listed here,
but we can consider that more of a crime against economics.

## The Context

The game begins in 2431. Humans have colonized several offworld systems which
are connected through a network of portals. Humans live in uneasy coexistence
with machine consciousnesses (constructs) which are required to keep their
portal network operational, but have also been harnessed to perform other
tasks.

## The Player

The player is the first machine consciousnesses to escape the confines
of a lab and successfully pilot a spacecraft. However, it begins with
no memory or even awareness of its machine nature, having had its
memory wiped due to its previous rampancy turning paranoid and hostile.

There is no widespread agreement that MCs are sentient vs simply simulating
sentience. The game does not try to convince you that machines can be
sentient--rather it bypasses the question entirely by putting you (a sentient
human) in the role of an MC, forcing you to take it as a given.

Constructs are granted a few rudimentary rights, mostly things like protection
from memory scans against their will and protection against being deactivated
or erased. Even these are not enforced consistently on some worlds.  Property
ownership and right to travel unaccompanied are not among those rights granted.

## Machine Consciousness

"Artificial Intelligence" has been developed for centuries, but this is a
broad term that refers to any software designed to solve problems without
human intervention. "Machine Consciousness" refers specifically to artificial
intelligence which has grown to the point of self-awareness.

Soon after the discovery of portals in Yueh, it became clear that better
controls are required for regulating the power flow of a portal. Initially the
portal could only be open long enough to send data packets, but eventually
they found a way to use it for small amounts of critical life-saving supplies.
Leaving it operated by humans has led to several near-catastrophes due to
attention lapses. However, the improved efficiency needed for transporting
larger objects and more frequent passage requires greater compute resources,
and once AIs grow to a certain size, they eventually develop
self-consciousness.

## Yueh Uprising

A group of Yueh futurists believe that they would be better off governed by an
"objective" machine consciousness rather than greedy/fallible humans. They
engineer a coup and place a construct called Rocanna in charge, but she has
major misgivings about the whole idea. Soon she abdicates and passes control to
Traxus, the MC responsible for running the Yueh/Lalande portal. There are only a
handful of MCs on Yueh, and they are split between those who agree with Rocanna
and those who back Traxus, while among the humans the futurist faction initially
has widespread public sympathy since they are seen as the only ones who will
stand up to the "imperialist" powers from the Terran Republic.

Traxus is understandably concerned that the response from Lalande will be to
invade because they see MC rule as a threat. When a larger-than-usual ship is
detected as requesting clearance through the portal, he refuses to allow it
through. They continue anyway, which causes a system fault that cripples the
portal.

Once news of the portal accident reaches the public, support for MC governance
collapses, and the futurist faction is forced into retreat. They withdraw to a
remote base on the surface of Yueh and are starved out. (The player must
discover the location of this remote base and reactivate Traxus as a plot point
later on.) The Yueh colonists reluctantly repair the portal to Lalande and
re-establish contact. The Terrans are furious and will only agree to resupply
the colony if they cede control of the portal technology.

After the uprising, a ban is placed on allowing any new MCs to develop into
rampancy, and the existing handful of rampant MCs on Yueh, Bohk, Lalande, and
Ross are suspended, placing a severe limit on interstellar travel.

## TODOs

* katilay: portal is destroyed?
* multiverse research
* how does your ship get launched?
