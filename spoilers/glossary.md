# Glossary

Members of the lab team won't use as much slang, but out when you're
talking with random folks on the newsgroups or job boards you'll hear
plenty of this.

* "panjandrums": government officials
* "Net of a million lies": the Internet
* "sixed": killed
* "crocla": mild expletive (feces)
* "nopping": idling
* "cons": construct
* "parse": understand
* "zero" (verb): wipe, erase
* "g-well": world
* "lock": portal
* "무": nothing, emptiness
* "bluescreen": derogatory term for machine consciousnesses
* "meatbag": derogatory term for humans

* Expletives
 * "Hreðjar Óðins": odin's balls
 * "พระ อาเจียน ลิง": holy monkey vomit
 * "พัด ขี้ ควาย": stir-fried water buffalo turds
 * "ท้อง เสีย ช้าง": elephant diarrhea
 * "Ma gavte la nata": get over yourself (extremely rude)

Much of the vocabulary is widely-used, but some turns of phrases (the
non-english ones) might just be used by certain characters. Nari uses the Thai
ones, for instance.
