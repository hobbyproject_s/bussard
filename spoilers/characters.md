# Companion Characters

Certain characters can be recruited to join you on your ship, provided
you have a life support upgrade installed. If you sell your life
support, any humans onboard will be forced to disembark, aborting any
passenger missions you may have accepted. If you have companions on
board, they will send you irate emails asking you to pick them back up
after you buy life support again.

Your only interaction with these characters is by email. They can
email you when certain plot points trigger (or maybe after a certain
amount of time has passed with no plot points triggering in the case
of hints) but you can't email them back beyond simple OK/NO replies.

## Nari Phouen <nari.phouen@starlink.net.sol>

She is an older freelance repair tech with an amateur interest in
historical computing. Gruff but kind at heart. She originally wants to
board your craft just to get passage to another system, but her
old-fashioned style means she won't get on board without meeting the
captain in-person first, which is obviously impossible in your
case. At first she thinks it's a scam, but when she does a more
detailed scan of your ship she realizes the truth (before you do).

Helps you realize who you are, decrypt your journals, and get a handle
on coding. Grew up on Mars and is a citizen of Sol.

She also offers to pretend to be the captain of the ship in any cases
where an un-manned ship would raise too many suspicions. She helps you
find Clay, and she is a little too trusting of Clay at first. She is
later really regretful about it. This causes her to be too suspicious
of Mila. Once she boards your ship, she is motivated more by curiosity
and a desire to help than her original goal of just getting home.

# Governments

This will make more sense if you read history.md first.

## Solar Union

Home of humanity, but regressed from its height. Suffered from having
portal delayed due to accident. Some believe the Ptolemy was sabotaged
on its mission to carry a portal to Sol.

Sol is ancient, and still hosts the majority of humankind who have never left
Earth. Many see it as backwards or quaint.

* [x] Earth
* [x] Mars
* [x] Newton Station
* [x] Nee Soon Station

## Terran Republic

Most wealthy and powerful, but only controls two systems, Lalande and
Ross. Formerly included Sol. Regulates portal technology.

* [x] Sungai (Lalande)
* [x] Bendera (Lalande)
* [x] Kuching Station (Ross)

## Principality of Istana

Independent station in a Terran system (Ross). Grandfathered into
independence early on. Isolated, wealthy, and small.

* [x] Istana (Ross)

## Kingdom of Bohk

Due to a lack of communication, developed fairly independently from
the Terran Republic. Has two internal portals under oversight from
Terrans. Helped Lalande re-establish contact with Katilay, and also
helped Yueh establish its Delta Pavonis colony.

* [x] Bohk Prime
* [x] Warnabu Station
* [x] Changlun (New Phobos)
* [x] Sutap (New Phobos)
* [x] Tirakir (Mecalle)

## Yueh

Source of where the portal tech was discovered. Runs two portals (Bohk
and Delta Pavonis) and has one portal (Yueh/Kowlu) run by Lalande,
which is a point of tension.

* [ ] Yueh Prime
* [ ] Da Kau Station
* [ ] Bata Beng (Kowlu)
* [ ] Sim Roen (Kowlu)
* [ ] Packsi (Delta Pavonis)

The portal to Delta Pavonis is secretly a multiportal; it can take you
to other locations if you have proper access.

## Republic of Katilay

Conflict-wracked even within a single system. Portal was faulty upon
arrival; contact with Lalande only recently re-established.

* [x] Katilay Prime
* [x] Tamada Station

## Tana Protectorates

Newest government, close ties still to Solar Union. Rich in
minerals. Terrans allow Sol to operate portals keeping Tana running,
but Tana is completely dependent upon Sol for food and Terrans for
transport, leaving it quite vulnerable.

* [x] Tana Prime (Tana)
* [x] Lioboro (Tana)
* [x] Kenapa Station (Tana)
* [x] Solotogo (Wolf 294)
* [x] Kembali Station (Luyten)
* [x] Merdeka Station (L 668-21)

Tana has the most worlds, but the smallest population.

## Human Worlds League

Originally the Terran Republic covered all inhabited worlds, but as
new colonies sprung up independently or declared independence, Bohk
and Yueh spearheaded the League soon after the war. Terran Republic
was also a founding member. Sol joined upon its independence, Katilay
joined several years after re-establishing connection. They oversee
trade agreements, enact spaceflight safety regulations, administrate a
unified currency, coordinate exploration efforts, and work to protect
peace. Nominally they aim to advance shared scientific progress, but
little of this happens in practice. The league is headquartered on
Bohk Prime.

TODO: Find a way to fit in significance of New Phobos, Kowlu, Delta Pavonis
OR: Yueh maybe doesn't need so many worlds

# Companies

* Ceres Shipyards (Sol)
  Responsible for all early long-haul colony ships and all Tana
  ones. All long-haul cargo ships made here. Located in the asteroid
  belt.

* Consolidated Shipping (Ross)
  Runs most inner-system routes. Starting to break into Tana lines,
  but only slowly.

* Songket Shipyards (Lalande)
  Responsible for most later colony ships, but also creates many
  interplanetary cargo ships.

* Kosaga Shipyards (small, Bohk)
  Started in order to create the New Phobos colony ship, but now only
  focuses on interplanetary ships. Created two colony ships for Yueh.

* Ares Mineral Company (Lalande)
  Dominant mining company.

* Allied Deliveries (Yueh)
  Handles most eastern delivery routes.

* Starlink (Sol)
  An ISP. (starlink.net.sol)

* WonderChannel
  Another ISP. (wonderchannel.net.sol)

* Leeft (Sol)
  Interplanetary and interstellar passenger runs.

* Aperture Technology (Lalande)
  Caretaker of portal artifacts, researches and maintains portal
  technology. Closely guards what little they know of how they work.

* Interstellar Communication Systems (Lalande)
  Works with Aperture to piggy-back data transmission on top of each
  portal open cycle.

* Luminous Enterprises (Yueh)
  Originally adapted portal technology for human construction. Barred
  from further portal development by treaty, but continuing it
  covertly. Can't colonize traditionally without constructing large
  colony ships, which would be noticed, so they are researching
  coldsleep as an alternative.

* Post-Terran Mining Company (Tana)
  The primary driver behind the colonization of Tana. Government of
  Tana is arguably a front for PTMC.

* Orolo Research (Sol)
  Responsible for early sublight drives, recently assisting Luminous
  with coldsleep research.

* GNO Project (Sol)
  Responsible for maintaining software infrastrucutre such as the Orb OS.

* TMRC (Bohk, Yueh)
  Loose association of hackers, mostly interested in cool onboard computer
  tricks, but also with some interest in security.

# Universities

...

# Spaceships

## Early Interplanetary ships

## Colonizers

Self-sufficient ships with crews numbering in the mid-hundreds. Upon
arrival the ships are disassembled and the parts used to construct
either planetary colonies or re-appropriated into orbital stations.

* First Wave: SS Lorentz (Lalande, 0.3c), SS Copernicus (Ross, 0.5c)
* Second Wave: SS Bradbury (Bohk, 0.6c),
  SS Eratosthenes † (New Phobos, 0.5c), SS Ptolemy (Yueh, 0.6c)
* Third Wave: SS Kepler † (Kowlu)
* Fourth Wave: SS Las Casas (Katilay), SS Cherenkov † (Mecalle),
  SS Sagan (Delta Pavonis)
* Fifth Wave: SS Pythagoras (Luyten), SS Planck (Tana),
  SS Rosen (Wolf), SS Archimedes (L 668-21)

† - ship from Bohk

From third-wave on they all have portals onboard and fly at 0.7c.

## Portal ships

Newer colonizers have portals on them, but existing colonies need
ships containing just the equipment needed to create the portal.

* Original portal ship: SS Ptolemy (converted from Yueh colonization)
 * Yueh->Lalande
 * Lalande->Sol (accident)
* Second portal ship: SS Euclid
 * Lalande->Ross
 * Bohk->New Phobos
 * Mecalle->Katilay
* Ptolemy Rescue ship: SS Goddard
* Bohk trade ship: SS Oberth
 * carries portal from Yueh back to Bohk

## Modern Interplanetary ships

Engines capable of lots of maneuvering around a star but not long-term
flight. They need significant battery if they are to activate a
portal. Mostly they carry cargo and passengers; occasionally
scientific equipment.

## Non-colonizing exploration ships? (automated?)

# Gutenberg Liberation Front

The Gutenberg Liberation Front is protesting the extension of
copyright term. They do this by posting chapters of public-domain
works into random news groups. Some of the works they use:

* The Man who was Thursday
* Meditations on Moloch
* The Song of Roland
* Some Doctorow novel?
* Shakespeare?
* The Grand Inquisitor by Dostoyevsky
* Kierkegaard?
* Leiningen vs the Ants?
