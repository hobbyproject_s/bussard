# Welcome!

If you're just getting started, you'll want to acquaint yourself with
the flight controls first. Right now you're reading this in the console,
which lets you interact with your ship's onboard computer. Flight mode
is toggled with ctrl-enter, so hit that now, then come back.

Use the arrow keys to fly around a bit, but keep an eye on the red
fuel gauge in the upper left. The faster you're going, the more fuel
(and time) it will take to slow back down. Your fuel will recharge,
but it takes time. You can't collide with anything, so don't
fear. Switch to flight mode now and try it out for a bit.

The green curve plots your estimated trajectory, and the box in the upper left
shows your velocity vector. Use the equals and minus keys, or the scroll wheel
on your mouse, to zoom in and out. The tab key cycles through all targets
linearly, while ctrl-tab selects the closest target.

Once you've got a feel for the controls and instrumentation, run this:

    man("quickstart2")
